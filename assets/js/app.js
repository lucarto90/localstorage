// Variables
const listaTweests = document.getElementById('lista-tweets');

// Event Listeners
eventListeners();

function eventListeners () {
    // Cuando se envia el formulario
    document.querySelector("#formulario").addEventListener('submit', agregarTweet);

    // Borrar tweets
    listaTweests.addEventListener('click', borrarTweet);

    // contenido cargado
    document.addEventListener('DOMContentLoaded', localStorageListo);
}

// Functions


// Añadir tweet del formulario
function agregarTweet (e) {
    e.preventDefault();
    // leer el valor del textarea
    const tweet = document.getElementById('tweet').value;

    verInfoTweets(tweet);

    // añadir a Local Storage
    agregarTweetLocalStorage(tweet);
}

// Elimina el tweet del DOM
function borrarTweet (e) {
    e.preventDefault();

    if (e.target.className === 'borrar-tweet') {
        e.target.parentElement.remove();
        // eliminar desde el local storage
        let tweet = e.target.parentElement.innerText;
        borrarTweetLocalStorage(tweet);
    }
}

// Agrega tweet a local storage
function agregarTweetLocalStorage (tweet) {
    let tweets;
    tweets = obtenerTweetsLocalStorage();

    // añadir el nuevo tweet
    tweets.push(tweet);

    // convertir de string a arreglo para local storage
    localStorage.setItem('tweets', JSON.stringify(tweets));
}

// Comprobar que haya elementos en local storage, retorna un array
function obtenerTweetsLocalStorage () {
    let tweets;

    // revisamos los valores de local storage
    if (localStorage.getItem('tweets') === null) {
        tweets = [];
    } else {
        tweets = JSON.parse(localStorage.getItem('tweets'));
    }

    return tweets;
}

// mostrar datos del local storage en la lista
function localStorageListo () {
    let tweets;

    tweets = obtenerTweetsLocalStorage();

    tweets.forEach((tweet) => {
        verInfoTweets(tweet);
    });
}

// Pintar la informacion de los tweets
function verInfoTweets (tweet) {
    // crear buttom para eliminar
    const botonBorrar = document.createElement('a');
    botonBorrar.classList = 'borrar-tweet';
    botonBorrar.innerText = 'X';

    // Crear elemento y añadir el contenido a la lista
    const li = document.createElement('li');
    li.innerText = tweet;
    // añade el boton de borrar al tweet
    li.appendChild(botonBorrar);
    // añade el tweet a la lista
    listaTweests.appendChild(li);
}

// eliminar el tweet del local storage
function borrarTweetLocalStorage (tweet) {
    let tweets, tweetBorrar;
    // eliminar la X del tweet
    tweetBorrar = tweet.substring(0, tweet.length - 1);

    tweets = obtenerTweetsLocalStorage();

    tweets.forEach((tweet, index) => {
        if (tweetBorrar === tweet) {
            tweets.splice(index, 1);
        }
    });

    localStorage.setItem('tweets', JSON.stringify(tweets));
}